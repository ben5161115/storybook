module.exports = {
    testMatch: ['**/*.test.js', '**/*.test.ts'],
    snapshotSerializers: ['jest-serializer-vue'],
    transform: {
      '^.+\\.vue$': '@vue/vue3-jest', 
      '^.+\\.tsx?$': 'ts-jest',   
    },
    globals: {
        'ts-jest': {
          tsconfig: 'tsconfig.json', 
        },
    },
    testEnvironment: 'jsdom',
    testEnvironmentOptions: {
      customExportConditions: ["node", "node-addons"],
   },
  };
  