import type { Meta, StoryObj } from '@storybook/vue3';
import RegisterForm from './SB-register-form.vue';
import { within, userEvent } from '@storybook/testing-library';
import { linkTo } from '@storybook/addon-links'; 


const customViewports = {
  kindleFire2: {
    name: 'Kindle Fire 2',
    styles: {
      width: '600px',
      height: '963px',
    },
  },
  kindleFireHD: {
    name: 'Kindle Fire HD',
    styles: {
      width: '533px',
      height: '801px',
    },
  },
};


const meta: Meta<typeof RegisterForm> = {
  title: 'RegisterForm',
  //viewport: { viewports: customViewports },
  component: RegisterForm,
  tags: ['autodocs'],
  argTypes: {
    labelPosition: { control: 'select', options: ['top', 'left'] },
    hideLabels: { control: 'boolean' },
    disabled: { control: 'array' },
  },
  args: {
    fields: ['name', 'email', 'password'],
    fieldTitles: ['Name', 'Email', 'Password'],
    placeholders: ['Enter your name', 'Enter your email', 'Enter your password'],
  },
};

export default meta;
type Story = StoryObj<typeof RegisterForm>;

export const DefaultForm: Story = {
  play: async ({ canvasElement }) => {
    // Find the input elements
    const nameInput = within(canvasElement).getByPlaceholderText('Enter your name');
    const emailInput = within(canvasElement).getByPlaceholderText('Enter your email');
    const passwordInput = within(canvasElement).getByPlaceholderText('Enter your password');
    const submitButton = within(canvasElement).getByText('Register');

    // Simulate user interactions
    await userEvent.type(nameInput, 'John Doe');
    await userEvent.type(emailInput, 'john.doe@example.com');
    await userEvent.type(passwordInput, 'password123');
    await userEvent.click(submitButton);

    
  },
  
};




export const LabelLeftOfInput: Story = {
    args: {
      fields: ['username', 'password', 'email'],
      labelPosition: 'left',
    },
    parameters: {
      design: {
        type: "figma",
        url: "https://www.figma.com/file/KtYBEvWZ9FRYzF0CnFPZk1/Untitled?type=design&node-id=0-1&mode=design&t=R5oHnkoPgLHDasRC-0",
      },
    },
  };
 
  
  export const NoLabelsJustPlaceholders: Story = {
    args: {
      fields: ['username', 'password', 'email'],
      hideLabels: true,
    },
  };
  
  export const CustomPlaceholders: Story = {
    args: {
      fields: ['username', 'password', 'email'],
      hideLabels: true,
      placeholders: ['Enter Username', 'Enter Password', 'Enter Email'],
    },
  };
  
  export const DisabledFields: Story = {
    args: {
      fields: ['username', 'password', 'email'],
      disabled: ['password'],
    },
  };
  
  export const AllParameters: Story = {
    args: {
      fields: ['username', 'password', 'email', 'first name'],
      fieldTitles: ['User Name', 'Password', 'Email Address'],
      placeholders: ['Enter Username', 'Enter Password', 'Enter Email'],
      labelPosition: 'left',
      hideLabels: false,
      disabled: ['password'],
    },
  };
  
  export const LabelAboveInput: Story = {
    args: {
      fields: ['username', 'password', 'email'],
      labelPosition: 'top', 
    },
  };


  export const GoToSecondButton  = () => ({
    template: '<button @click="goToSecond">Go to "Second"</button>',
    methods: {
      goToSecond: () => linkTo('RegisterForm', 'GoToFirstButton')(),
    },
  });
  
  export const GoToFirstButton = () => ({
    template: '<button @click="goToFirst">Go to "First"</button>',
    methods: {
      goToFirst: () => linkTo('RegisterForm', 'GoToSecondButton')(),
    },
  });
  
  
