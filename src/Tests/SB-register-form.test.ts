import { render } from '@testing-library/vue';
import RegisterForm from '../stories/SB-register-form.vue';
import '@testing-library/jest-dom';

test('renders fields with correct labels and placeholders', () => {
  const { getByPlaceholderText, getByLabelText } = render(RegisterForm, {
    props: {
      fields: ['username', 'password', 'email'],
      placeholders: ['Enter Username', 'Enter Password', 'Enter Email'],
    },
  });

  expect(getByLabelText('username')).toBeInTheDocument();
  expect(getByPlaceholderText('Enter Username')).toBeInTheDocument();
  expect(getByPlaceholderText('Enter Password')).toBeInTheDocument();
  expect(getByPlaceholderText('Enter Email')).toBeInTheDocument();
});

test('disables specified fields', () => {
  const { getByLabelText } = render(RegisterForm, {
    props: {
      fields: ['username', 'password', 'email'],
      disabled: ['password'],
    },
  });

  expect(getByLabelText('password')).toBeDisabled();
});
