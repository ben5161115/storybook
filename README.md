
Storybook provides a test runner that allows you to setup automatic ui tests that can run from the ci/cd ensuring a consistent design throughout development.

It also has some intergrations with Jest for more complex testing such as some of whats listed below:

Tests:

Visual Testing: With tools like Chromatic, Storybook enables visual regression testing, capturing screenshots of stories and comparing them against baselines to detect visual changes.

Accessibility Testing: The Accessibility addon (@storybook/addon-a11y) in Storybook allows developers to check components for accessibility issues, ensuring compliance with standards like WCAG.

Interaction Testing: Introduced in Storybook 6.4, Interaction Testing allows developers to write tests simulating user interactions within Storybook itself, verifying component functionality.

Integration with Jest: Storybook can be integrated with Jest for snapshot testing. Using the Storyshots addon, developers can automatically generate snapshot tests for all stories, leveraging Jest's powerful testing framework.

Storybook Test Runner: Powered by Jest and Playwright, the Storybook test runner turns stories into executable tests that run in a live browser, providing an automated way to test components.

Importing Stories in Other Tests: Storybook allows developers to import stories into other testing tools like Jest, Testing Library, Puppeteer, Cypress, and Playwright, ensuring consistency and saving time in writing tests.




ideas:

The auto docs add on can be utilised to create a library of documented components meaning anyone can go onto the library and quickly understand what each of the components do and how they look/behave greatly decreasing the amount of time it takes to setup a new project.

Once a large library has been created with variations of each, a script could be made that asks some questions and then generates a vue project with the components based on the answers to the script, meaning development couold start with most of the frontend scafolding already generated. 

Once tests are written, since the components are resuable they will work in any project, so all future projects would come with the ui testing form the start allowing for a consistent design throughout development.

Storybook can be used for quick prototyping. You can create and test new features/components in isolation before integrating them into the main application.



Useful Addons:

Versions: Acts like git for the components in your storybook, allowing you to navigate through different versions of your components.

Viewport: Enables the display of stories in different sizes and layouts, simulating various devices.

emit events

Interactive Documentation: With the "Controls" addon, you can create an interactive playground for each component. This allows users to see how components behave with different props and states, making it easier to understand their functionality.

Languages:  "i18n" addon can be used to preview components in different languages. This


Figma Integration

Storyboard comes with a Figma plugin that allows you to link Storybook stories to Figma designs, helping to compare implementation to design spec easily. The addon `@storybook/addon-designs` allows for live viewing of a figma file next to any of the Storybook stories, all you need to do is add the link to the file and it will show up beside the story in the design tab. 

Figma also comes with the `Storybook Connect` plugin that allows you to view your Stories in Figma, you just need to publish your story to chromatic and provide the link to Figma and then it can be viewed within a tab in figma along side your design.



Testing

Error: Vue is not defined.

Solution:  
 ```typescript
            testEnvironmentOptions: {
                customExportConditions: ["node", "node-addons"],
            },
 ```


 Interaction Testing
 npm install @storybook/addon-interactions


 The interactive testing allows you to define a set of 'instructions'/'interactions' with the components. Then, when you view the component in the browser with storyboard, it will run the test and carry out the instructions, allowing the user to visually see if it works correctly. This is a manual testing solution, not automated. To get the tests running you just install the addon and the plugins listed below, then define the test within the story the component to test.

 install: Jest28, @vue/vue-jest28, @ts-jest28, @testing-library/jest-dom


 Testing with Jest: Storybook comes with a jest-addon allowing for the creation of automated tests. 

 Accessibility Testing: Storyboard comes with a very simple to use accessibility addon, you just install it into the project and it will add a tab to the storybook browser to view the results of the accessibility tests the add-on comes with (based on WCAG rules)(from the Axe accessibility testing library). You cannot create your own custom accessibility tests as part of the addon, but you can create your own jest tests with the same purpose, they just wont appear in the Accessibility tab on storybook.

 Snapshot tests (uses Jest): The storyshots addon allows storybook to take snapshots of your components when the test is ran, the next times the test is ran it will take new snapshots and compare to the original, if there are any changes it will notify you and ask if they were intenetional, if they were then it will tell you to run a command to set them as the new snapshots for future tests (npx jest -u). It is very easy to setup, you just need to install it into the project and create a .test.ts file with the lines: 
 ```typescript
    import initStoryshots from '@storybook/addon-storyshots';
    initStoryshots();
``` 
Also, need to add this to jest config:     testEnvironment: 'jsdom',
Along with installing it


Viewport addon: install addon -> add to main.ts -> add this line to story main meta data:   `viewport: { viewports: customViewports }` and then create a viewport object: 
```typescript
const customViewports = {
  kindleFire2: {
    name: 'Kindle Fire 2',
    styles: {
      width: '600px',
      height: '963px',
    },
  },
  kindleFireHD: {
    name: 'Kindle Fire HD',
    styles: {
      width: '533px',
      height: '801px',
    },
  },
};
```


Links addon: cant get working?


Measure addon: 